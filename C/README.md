# TeachingProgramming

Just a repo where I help friends learn how to program

# DanaWildy

https://www.youtube.com/watch?v=YOLN-t09-tM

# bitbucket
https://bitbucket.org

# atlassian
https://www.atlassian.com/

/* Intro
Dana,

	You will begin by learning the C programming language.  You have already 
	started learning C# so this will seem like a step backwards for you.  Unlike 
	C#, C is NOT object oriented and there are alot of tools that are not 
	available to you in this langauge. 
	
	This file will serve as a general information file for you to reference during
	this course.  I will update it from time to time but I will not add time or 
	date stamps to my updates unless i feel the need to.  
	
	In order to know if it has changed check the file status in source tree and
	it should be able to show you the 'diff' of the versions. 
	
	Good Luck!
	Mike

end intro	*/

/* Bitbucket, atlassian and git

	you will be using sourc tree to manage the uploading of assignments.  Unlike
	traditional universities you will not email your assignments but rather 
	'check' them into the 'repository' onto your 'branch' for me to 'merge' onto
	the 'master'.  
	
	As you can see there are lots of new terms in that paragraph. I'm not going 
	to explain them here, you will have to learn them.  Ask questions, do 
	research and take notes.

end git stuff	*/